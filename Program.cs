﻿using System;

internal class Program
{
  private static void Main(string[] args)
  {
    File file1 = new File("otus.md");
    File file2 = new File("anton.vc");
    Directory root = new Directory("");
    Directory dir1 = new Directory("home");
    Directory dir2 = new Directory("lesson");
    root.AddChild(dir1);
    dir1.AddChild(dir2);
    dir2.AddChild(file1);
    dir2.AddChild(file2);

    var cmd = new Client();
    cmd.MkDir(root, "work");
    cmd.MkDir(root, "home/test");

    var link1 = new Link("md", file1);

    // отойдем от использования клиента
    file1.SetContent("Patterns lesson");
    file2.SetContent("Speaker");

    cmd.Cat(link1);
    cmd.Cat(dir2);
  }
}
public abstract class Visitor
{
  public abstract void Visit(File file);
  public abstract void Visit(Directory file);
  public abstract void Visit(Link file);
}

public class CatVisitor : Visitor
{
  public override void Visit(File file)
  {
    Console.WriteLine(file.GetContent());
  }

  public override void Visit(Directory dir)
  {
    Console.WriteLine("cannot cat directory");
  }

  public override void Visit(Link link)
  {
    link.Subject.Accept(this);
  }
}

public class Client
{
  public void MkDir(Directory dir, string name)
  {
    var s = name.Split('/', StringSplitOptions.RemoveEmptyEntries);
    if (s.Length == 1)
    {
      dir.AddChild(new Directory(name));
    }
    else
    {
      var node = dir.GetChild(s[0]);
      if (node != null)
      {
        // а что делать если это не так?
        MkDir(node as Directory, name.Substring(name.IndexOf('/')));
      }
    }
  }

  public void Cat(Node node)
  {
    var visitor = new CatVisitor();
    node.Accept(visitor);
  }
}
