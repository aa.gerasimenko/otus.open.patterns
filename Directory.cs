﻿using System.Collections.Generic;

public class Directory : Node
{
  public Directory(string name) : base(name)
  {
    Nodes = new List<Node>();
  }

  public List<Node> Nodes { get; set; }

  public override void Accept(Visitor visitor)
  {
    visitor.Visit(this);
  }

  public override void AddChild(Node child)
  {
    Nodes.Add(child);
  }

  public override Node GetChild(string name)
  {
    return Nodes.Find(x => x.Name == name);
  }

  public override string GetContent()
  {
    throw new System.NotImplementedException();
  }

  public override void RemoveChild(Node child)
  {
    Nodes.Remove(child);
  }

  public override void SetContent(string content)
  {
    throw new System.NotImplementedException();
  }
}