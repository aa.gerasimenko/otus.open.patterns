﻿using System;

public class File : Node
{
  private string _content;

  public File(string name) : base(name)
  {
  }

  public override void Accept(Visitor visitor)
  {
    visitor.Visit(this);
  }

  public override void AddChild(Node child)
  {
    throw new NotImplementedException();
  }

  public override Node GetChild(string name)
  {
    throw new NotImplementedException();
  }

  public override string GetContent()
  {
    return _content;
  }

  public override void RemoveChild(Node child)
  {
    throw new NotImplementedException();
  }

  public override void SetContent(string content)
  {
    _content = content;
  }
}