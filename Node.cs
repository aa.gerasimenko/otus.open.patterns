﻿public abstract class Node
{
  protected Node(string name)
  {
    Name = name;
  }

  public string Name { get; set; }

  public abstract void Accept(Visitor visitor);

  //общий интерфейс
  public abstract void AddChild(Node child);

  public abstract Node GetChild(string name);

  public abstract string GetContent();

  public abstract void RemoveChild(Node child);

  public abstract void SetContent(string content);
}