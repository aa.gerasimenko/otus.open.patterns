﻿using System;

public class Link : Node
{
  public Link(string name, Node subject) : base(name)
  {
    Subject = subject;
  }

  public Node Subject { get; private set; }

  public override void Accept(Visitor visitor)
  {
    visitor.Visit(this);
  }

  public override void AddChild(Node child)
  {
    Subject.AddChild(child);
  }

  public override Node GetChild(string name)
  {
    return Subject.GetChild(name);
  }

  public override string GetContent()
  {
    return Subject.GetContent();
  }

  public override void RemoveChild(Node child)
  {
    Subject.RemoveChild(child);
  }

  public override void SetContent(string content)
  {
    throw new NotImplementedException();
  }
}